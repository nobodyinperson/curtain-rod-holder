// clang-format off
use <threads-scad/threads.scad>;
// clang-format on

/* [Rods] */
curtain_rod_diameter = 15.5;
wall_rod_diameter = 12.2;

/* [Curtain Rod Tube] */
curtain_rod_tube_wall_thickness = 5;
curtain_rod_tube_length = 100;
curtain_rod_tube_tip_length = 20;
curtain_rod_tube_tip_min_wall_thickness = 1;

/* [Wall Rod Hook] */
wall_rod_hook_thickness = 5;
wall_rod_hook_shaft_length = 30;
wall_rod_hook_shaft_diameter = 12;
wall_rod_hook_shaft_shape = "square"; // ["square","circle","screw"]
wall_rod_hook_width = wall_rod_hook_shaft_shape == "square"
                        ? wall_rod_hook_shaft_diameter / sqrt(2)
                        : wall_rod_hook_shaft_diameter;
wall_rod_hook_shaft_screw_tooth_angle = 55;

/* [Display] */

slice_in_preview = true;

/* [Precision] */
$fa = $preview ? 1 : 0.5;
$fs = $preview ? 1 : 0.5;
tolerance = 0.1;
epsilon = 0.1;

module
WallHookShaftScrewHoleIfNeeded()
{
  if (wall_rod_hook_shaft_shape == "screw") {
    ScrewHole(outer_diam = wall_rod_hook_shaft_diameter,
              height = wall_rod_hook_shaft_length + epsilon,
              tooth_angle = wall_rod_hook_shaft_screw_tooth_angle) children();
  } else {
    children();
  }
}

module
WallHookShaft()
{
  if (wall_rod_hook_shaft_shape == "square") {
    translate([ 0, 0, wall_rod_hook_shaft_length / 2 ]) cube(
      [
        wall_rod_hook_shaft_diameter / sqrt(2) - tolerance,
        wall_rod_hook_shaft_diameter / sqrt(2) - tolerance,
        wall_rod_hook_shaft_length
      ],
      center = true);
  } else if (wall_rod_hook_shaft_shape == "circle") {
    cylinder(d = wall_rod_hook_shaft_diameter - tolerance,
             h = wall_rod_hook_shaft_length);
  } else if (wall_rod_hook_shaft_shape == "screw") {
    ScrewThread(outer_diam = wall_rod_hook_shaft_diameter,
                height = wall_rod_hook_shaft_length,
                tooth_angle = wall_rod_hook_shaft_screw_tooth_angle);
  }
}

module
MoveToWallHookCenter()
{
  translate([ 0, 0, -(wall_rod_diameter + 2 * wall_rod_hook_thickness) / 2 ])
    rotate([ 0, 90, 0 ]) children();
}

module
WallHook()
{
  // the fixture inside the curtain rod tube
  WallHookShaft();
  difference()
  {
    hull()
    {
      // the hook itself
      MoveToWallHookCenter()
        cylinder(d = wall_rod_diameter + 2 * wall_rod_hook_thickness,
                 h = wall_rod_hook_width - tolerance,
                 center = true);
      // smooth transition to the shaft
      linear_extrude(epsilon) projection(cut = true) WallHookShaft();
    }
    // remove main hole for the wall rot
    MoveToWallHookCenter() cylinder(d = wall_rod_diameter + tolerance,
                                    h = wall_rod_hook_width + epsilon,
                                    center = true);
    // remove lower part for snapin
    hull()
    {
      translate([ 0, wall_rod_diameter / 2, 0 ]) MoveToWallHookCenter()
        cylinder(d = wall_rod_diameter + tolerance,
                 h = wall_rod_hook_width + epsilon,
                 center = true);
      translate([ 0, wall_rod_diameter, 0 ]) MoveToWallHookCenter()
        cylinder(d = wall_rod_diameter + tolerance,
                 h = wall_rod_hook_width + epsilon,
                 center = true);
    }
  }
}

intersection()
{

  if (slice_in_preview && $preview) {
    total_length = curtain_rod_tube_length + wall_rod_hook_shaft_length +
                   wall_rod_diameter + 2 * wall_rod_hook_thickness;
    total_width = curtain_rod_diameter + 2 * curtain_rod_tube_wall_thickness;
    translate([
      0,
      -total_width * 1.1 / 2,
      -(wall_rod_diameter + 2 * wall_rod_hook_thickness) * 1.1
    ]) cube([ total_width * 1.1, total_width * 1.1, total_length * 1.1 ]);
  }

  union()
  {

    // the curtain rod just for demonstration
    % translate([ 0, 0, wall_rod_hook_shaft_length ])
        cylinder(d = curtain_rod_diameter, h = curtain_rod_tube_length * 1.5);

    // the tube holding the curtain rod
    difference()
    {
      WallHookShaftScrewHoleIfNeeded() hull()
      {
        // the base tube
        cylinder(d = curtain_rod_diameter + curtain_rod_tube_wall_thickness +
                     tolerance,
                 h = curtain_rod_tube_length - curtain_rod_tube_tip_length);
        // reduce diameter for the tip
        translate(
          [ 0, 0, curtain_rod_tube_length - curtain_rod_tube_tip_length ])
          cylinder(d = curtain_rod_diameter +
                       curtain_rod_tube_tip_min_wall_thickness + tolerance,
                   h = curtain_rod_tube_tip_length);
      }
      // the hole in the tube for the curtain rod
      translate([ 0, 0, wall_rod_hook_shaft_length - epsilon / 2 ])
        cylinder(d = curtain_rod_diameter + tolerance,
                 h = curtain_rod_tube_length + epsilon);

      // the hole in the tube for the wall rod hook shaft
      translate([ 0, 0, -epsilon / 2 ])
      {
        if (wall_rod_hook_shaft_shape == "circle") {
          cylinder(d = wall_rod_hook_shaft_diameter + tolerance,
                   h = wall_rod_hook_shaft_length + epsilon);
        } else if (wall_rod_hook_shaft_shape == "square") {
          translate([ 0, 0, wall_rod_hook_shaft_length / 2 ]) cube(
            [
              wall_rod_hook_shaft_diameter / sqrt(2) + tolerance,
              wall_rod_hook_shaft_diameter / sqrt(2) + tolerance,
              wall_rod_hook_shaft_length +
              epsilon
            ],
            center = true);
        }
      }
    }
    translate([ 0, 0, -tolerance ]) WallHook();
  }
}
